# README

Questo codice è una semplice applicazione di quanto presentato durante il MeetUp del gruppo [Scala Treviso](http://www.meetup.com/it/Treviso-Scala-Meetup) il 25/06/2015, nel talk intitolato "Scala ed Rx in Pratica".

Nella versione attuale, si ottiene un jar eseguibile che, leggendo da un file di configurazione delle credenziali ed 
un percorso locale, copia tutti i file che trova localmente sul bucket S3 indicato, usando l'account configurato.

# Compilazione ed uso

Il progetto viene costruito con [sbt](http://www.scala-sbt.org/). Basta eseguire:


```
#!bash

sbt assembly
```

per ottenere nella directory target/scala-2.11 il jar eseguibile.
Il jar può essere lanciato in questo modo:


```
#!bash

java -jar scalaTreviso20150625-assembly-0.1.jar -f config.json
```

Se non vengono specificati i parametri corretti viene stampata una stringa esplicativa.

# Configurazione

Il file config.json è un esempio del formato della configurazione. I dati necessari sono:

* *directoryIn* percorso locale da dove prendere i file
* *s3Out* configurazione del client AWS S3, comprensiva di
+ * *key* chiave AWS
+ * *secret* segreto corrispondente alla chiave
+ * *bucket* bucket AWS S3 (già esistente) su cui caricare i file