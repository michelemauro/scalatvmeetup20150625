name := "scalaTreviso20150625"

version := "0.1"

scalaVersion := "2.11.6"

libraryDependencies ++= Seq(
  "com.github.scopt" %% "scopt" % "3.3.0",
  "com.fasterxml.jackson.core" % "jackson-core" % "2.5.3",
  "com.fasterxml.jackson.core" % "jackson-annotations" % "2.5.3",
  "com.fasterxml.jackson.core" % "jackson-databind" % "2.5.3",
  "com.fasterxml.jackson.module" %% "jackson-module-scala" % "2.5.2",
  "io.reactivex" %% "rxscala" % "0.25.0",
  "org.scalatest" %% "scalatest" % "2.2.4" % "test",
  // tecnologie di collegamento
  "io.atlassian.aws-scala" %% "aws-scala-s3"  % "2.0.1",
   // resolve conflicts
  "org.scala-lang" % "scala-reflect" % "2.11.6",
  "org.scala-lang.modules" %% "scala-xml" % "1.0.3"
)

resolvers += Resolver.sonatypeRepo("public")

resolvers += "atlassian-public" at "https://maven.atlassian.com/content/groups/public/"
