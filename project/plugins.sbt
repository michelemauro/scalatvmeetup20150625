logLevel := Level.Warn

addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.13.0")
addSbtPlugin("org.scalastyle" %% "scalastyle-sbt-plugin" % "0.6.0")
addSbtPlugin("com.timushev.sbt" % "sbt-updates" % "0.1.8")

resolvers += "sonatype-releases" at "https://oss.sonatype.org/content/repositories/releases/"