import drivers.{DirectoryName, RemoteDriver, LocalDriver, FileName}
import treeWalker.TreeAction


/**
 * Risultato di un'operazione
 * @param docid risultato dell'operazione di caricamento
 * @param bytes bytes trasferiti
 */
case class Result(docid: String, bytes: Long)

/**
 * Rappresentazione delle azioni di caricamento da contenuti locali a contenuti remoti
 */
trait Action extends TreeAction[Result]

/**
 * Rappresenta un contenuto da caricare remotamente
 *
 * @param content file da caricare
 * @param local driver per la comunicazione con il sistema locale
 * @param remote driver per la comunicazione con il sistema remoto
 */
case class ContentUpload(content: FileName, local: LocalDriver, remote: RemoteDriver)
  extends Action {

  override def visit: (Result, List[Action]) = {
    val file= local.file(content)
    val res= Result(remote.upload(file, content), file.length())
    (res, List())
  }

  override def toString = s"ContentUpload(${content.path} ${content.namePart})"
}

/**
 * Rappresenta una directory locale il cui contenuto va caricato remotamente
 */
case class Directory(name: DirectoryName, local: LocalDriver, remote: RemoteDriver) extends Action {
  override def visit: (Result, List[Action]) = {
    val (localDirs, localFiles) = local.files(name)
    (Result("", 0), localFiles.map(ContentUpload(_, local, remote)) ++ localDirs.map(Directory(_, local, remote)))
  }

  override def toString = s"Directory(${name.path} ${name.namePart})"
}