package drivers

import java.io.File

import com.amazonaws.regions.{Region, Regions}
import com.amazonaws.services.s3.AmazonS3Client
import io.atlassian.aws.{AmazonClient, AmazonClientConnectionDef, Credential}

case class S3account(key: String, secret: String, bucket: String)

/**
 * Driver per usare S3 come destinazione remota
 */
class S3RemoteDriver(key: String, secret: String, bucket: String) extends RemoteDriver {

  val config = AmazonClientConnectionDef(socketTimeoutMs = Some(60000),
    connectionTimeoutMs = Some(60000), maxErrorRetry = Some(5), maxConnections = Some(16),
    proxyHost = None, proxyPort = None, region = Some(Region.getRegion(Regions.EU_CENTRAL_1)),
    endpointUrl = None, credential = Some(Credential.static(key, secret)))

  val client= AmazonClient.withClientConfiguration[AmazonS3Client](config)

  def upload(file: File, fileName: FileName) = {
    client.putObject(bucket, fileName.namePart, file).getContentMd5
  }

}
