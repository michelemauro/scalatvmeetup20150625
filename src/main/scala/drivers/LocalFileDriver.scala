package drivers

import java.io.File

// nuova gerarchia
trait NamePath {
  val path: String
  val namePart: String
}

/**
 * Nome di un file (remoto o locale)
 *
 * @param path percorso rappresentato
 * @param namePart nome rappresentato
 */
case class FileName(path: String, namePart: String) extends NamePath

object FileName {
  def apply(path: DirectoryName, name: String): FileName = FileName(path.path + path.separator + path.namePart, name)
}

/**
 * Nome di una cartella (remota o locale)
 *
 * @param path percorso rappresentato
 * @param namePart nome rappresentato
 */
case class DirectoryName(path: String, namePart: String) extends NamePath {
  val separator = "/"
  val prefix = {
    val pf = path + separator
    if (pf.startsWith("//")) pf.substring(1) else pf
  }

  def dir(name: String): DirectoryName = DirectoryName(prefix + namePart, name)

  def content(name: String): FileName = FileName(prefix + namePart, name)

}

/**
 * Driver per il filesystem locale
 */
class LocalFileDriver(root: DirectoryName, fileNameFilter: File => Boolean) extends LocalDriver {

  def realPath(path: FileName) = root.path + root.separator + root.namePart + root.separator + path.path +
    root.separator + path.namePart

  def realPath(path: DirectoryName) = root.path + root.separator + root.namePart + root.separator + path.path +
    root.separator + path.namePart

  /**
   * Ottieni il file locale indicato dal percorso
   * @param path Nome del contenuto locale
   * @return File indicato dal nome
   */
  override def file(path: FileName): File = {
    val rp = realPath(path)
    val res = new File(rp)
    if (!res.exists()) throw new RuntimeException(s"requested not esisting file: $rp")
    res
  }

  /**
   * Legge i contenuti della cartella locale
   * @param name cartella locale da esaminare (corrispondente ad un nome di cartella remoto)
   * @return elenco delle sottocartelle e dei file contenuti
   */
  override def files(name: DirectoryName): (List[DirectoryName], List[FileName]) = {
    val contents = dirContents(new File(realPath(name)))
    val subfolders = contents.filter(_.isDirectory).map({
      f: File => name.dir(f.getName)
    })
    val files = contents.filter(_.isFile).filter(fileNameFilter).map({
      f: File => name.content(f.getName)
    })
    (subfolders, files)
  }

  def dirContents(f: File) = if (f.listFiles() != null)
    f.listFiles().toList
  else
    List()
}
