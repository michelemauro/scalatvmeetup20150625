package drivers

import java.io.File

/**
 * Driver del lato locale della sincronizzazione
 */
trait LocalDriver {
  /**
   * Ottieni il file locale indicato dal percorso
   * @param path Nome del contenuto locale
   * @return File indicato dal nome
   */
  def file(path: FileName): File

  /**
   * Legge i contenuti della cartella locale
   * @param name cartella locale da esaminare (corrispondente ad un nome di cartella remoto)
   * @return elenco delle sottocartelle e dei file contenuti
   */
  def files(name: DirectoryName): (List[DirectoryName], List[FileName])
}

/**
 * Driver del lato remoto della sincronizzazione
 */
trait RemoteDriver {
  /**
   * Carica un contenuto sul sistema remoto
   *
   * @param file File che fornisce i byte da caricare
   * @param fileName Nome (senza il percorso) del contenuto remoto
   */
  def upload(file: File, fileName: FileName): String

}

