import java.io.{PrintWriter, BufferedInputStream, File, FileInputStream}
import java.util.Date

import com.fasterxml.jackson.core.JsonFactory
import com.fasterxml.jackson.databind.{DeserializationFeature, ObjectMapper}
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper
import drivers._
import treeWalker._

/**
 * Configurazione di esecuzione
 * @param configFile nome del file di configurazione
 * @param dryRun effettua un dryrun invece di un'esecuzione reale
 * @param threads numero di threads da utilizzare
 */
case class RunConfig(configFile: File = new File("."), dryRun: Boolean = false, threads: Int = 2)

case class Config(directoryIn: String, s3Out: S3account)

case class Drivers(filesys: LocalFileDriver, s3out: S3RemoteDriver)

/**
 * Driver principale
 */
object Main extends App {

  val parser = new scopt.OptionParser[RunConfig]("scala-s3-uploader") {
    head("scala-s3-uploader", "0.1")
    opt[File]('f', "config") required() valueName "<config file>" action { (x, c) =>
      c.copy(configFile = x)
    } text "path to the configuration file"
    opt[Unit]('d', "dryRun") action { (x, c) =>
      c.copy(dryRun = true)
    } text "dryRun: check local and remote files, but don't upload anything"
    opt[Int]('t', "threads") valueName "<number of threads to use>" action { (x, c) =>
      c.copy(threads = x)
    } text "use the specified number of threads to work concurrently; default 2"
  }

  parser.parse(args, RunConfig()) match {
    case Some(config) =>
      launch(config)
    case None =>
      "wrong arguments"
  }

  def launch(config: RunConfig): Unit = {

    // lettura della configurazione
    lazy val mapper = {
      val factory = new JsonFactory()
      val mapper = new ObjectMapper(factory) with ScalaObjectMapper
      mapper.registerModule(DefaultScalaModule)
      mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
      mapper
    }
    val cfg = mapper.readValue[Config](new BufferedInputStream(new FileInputStream(config.configFile)))

    val fsIn = new LocalFileDriver(DirectoryName(cfg.directoryIn, ""), {
      f => !(f.getName().startsWith(".DS_") || f.getName.equals("Thumbs.db"))
    })
    val s3Out = new S3RemoteDriver(cfg.s3Out.key, cfg.s3Out.secret, cfg.s3Out.bucket)

    // setup dell'esecutore
    val walker = TreeWalker[Result](config.threads).traverse(
      List(Directory(DirectoryName("", ""), fsIn, s3Out))).publish

    // emettitore dei log
    val logRadix = "ws_%tY%<tm%<td-%<tH%<tM%<tS".format(new Date())
    val localLog = new PrintWriter(new File(logRadix + "_local.csv"))
    localLog.println("TimeStamp\tName\tSize\tTime")
    walker.subscribe(_ match {
      case TreeActionEnd(TreeResult(source, Result(name, bytes), time), _) =>
          localLog.println(f"${new Date()}%tY%<tm%<td-%<tH%<tM%<tS" +
            s"\t${name}\t${bytes}\t${time}")
      case _ => Nil
    })

    // log a console
    walker.subscribe(_ match {
      case TreeActionEnd(TreeResult(source, Result(name, bytes), time), _) =>
        println(f"${new Date()}%tY%<tm%<td-%<tH%<tM%<tS " +
          s"completata ${source} in ${time}ms")
      case TreeActionStart(source, agent) =>
        println(f"${new Date()}%tY%<tm%<td-%<tH%<tM%<tS " +
          s"iniziata ${source} da parte di ${agent}")
      case TreeActionQueue(source) =>
        println(f"${new Date()}%tY%<tm%<td-%<tH%<tM%<tS " + s"accodata ${source}")
      case _ => Nil
    })

    // sommatori dei file
    val doneResults = walker.filter({
      case TreeActionEnd(_, _) => true
      case _ => false
    }).map {
      case TreeActionEnd(TreeResult(source, done, _), _) => done
    }

    var files = 0L
    doneResults.map(r => 1L).sum.subscribe(files = _)

    var bytes = 0L
    doneResults.map(_.bytes).sum.subscribe(bytes = _)

    // chiusura di sicurezza del log
    walker.subscribe( te => Nil , e => {
      println("Error! ${e.getMessage}")
      localLog.flush()
      localLog.close()
    }, () => {
      println("Completed.")
      localLog.flush()
      localLog.close()
    })

    Runtime.getRuntime().addShutdownHook(new Thread() {
      override def run(): Unit = {
        localLog.println("Interrupted!")
        localLog.flush()
        localLog.close()
      }
    });

    walker.connect

    println(s"Eseguite $files operazioni per ${hrSize(bytes)} byte totali.")
  }

  def hrSize(size: Long) = {
    val unit = 1024
    if (size < unit)
      size + "b"
    else {
      val exp = (Math.log(size) / Math.log(unit)).toInt
      val pre = "kMGTPE".charAt(exp - 1)
      "%.2f %sb".format(size / Math.pow(unit, exp), pre)
    }
  }

}