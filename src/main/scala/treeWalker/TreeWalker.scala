package treeWalker

import java.util.concurrent.{ExecutorService, Executors, TimeUnit}

import rx.lang.scala.{Observable, Subscriber}

import scala.annotation.tailrec
import scala.concurrent.{ExecutionContext, Future, Promise}
import scala.util.Try

/**
 * Nodo dell'albero. Deve fornire il risultato della visita del nodo, e l'elenco dei nodi figli
 * @tparam R tipo del risultato della visita sul nodo
 */
trait TreeAction[R] {
  def visit: (R, List[TreeAction[R]])
}

/**
 * Risultato della visita
 * @tparam R tipo del risultato
 * @param source azione originale
 * @param result risultato dell'esecuzione
 * @param time tempo impiegato
 */
case class TreeResult[R](val source: TreeAction[R], val result: R, val time: Long)

/**
 * Evento di visita di un nodo
 * @tparam R tipo del risultato della visita
 */
trait TreeEvent[R]

/**
 * Ingresso in un nodo
 * @param action azione originale
 * @param agent agente che sta eseguendo l'azione
 * @tparam R tipo del risultato della visita
 */
case class TreeActionStart[R](action: TreeAction[R], agent: String) extends TreeEvent[R]

/**
 * Nodo completato
 * @param action risultato della visita
 * @param agent agente che ha eseguito l'azione
 * @tparam R tipo del risultato della visita
 */
case class TreeActionEnd[R](action: TreeResult[R], agent: String) extends TreeEvent[R]

/**
 * Accodamento di un nodo per l'esecuzione
 * @param action tipo dell'azione da eseguire
 * @tparam R tipo del risultato della visita
 */
case class TreeActionQueue[R](action: TreeAction[R]) extends TreeEvent[R]


/**
 * Percorre un albero breadth-first, restituendo una lista di risultati.
 * L'esecuzione è osservabile.
 */
class TreeWalker[R](executorService: ExecutorService) {

  type ResultRest[R] = (TreeResult[R], List[TreeAction[R]])
  type FutureResultRest[R] = Future[ResultRest[R]]

  implicit val executionContext = ExecutionContext.fromExecutorService(executorService)

  /**
   * Data una azione eseguibile, ritorna un Future che la eseguirà
   * @param arg Azione da eseguire
   * @return Azione eseguita e suo risultato
   */
  def buildFuture(arg: TreeAction[R], subscriber: Subscriber[TreeEvent[R]]): FutureResultRest[R] = Future {
    val threadname = Thread.currentThread().getName
    subscriber.onNext(TreeActionStart(arg, threadname))
    val start = System.currentTimeMillis()
    val result = arg.visit
    val done = System.currentTimeMillis() - start
    val res: TreeResult[R] = TreeResult(arg, result._1, done)
    subscriber.onNext(TreeActionEnd(res, threadname))
    (res, result._2)
  }

  /**
   * "Select" off the first future to be satisfied. Return this as a
   * result, with the remainder of the Futures as a sequence.
   *
   * @param fs a scala.collection.Seq
   * @author Victor Klang (https://gist.github.com/4488970)
   */
  def select[A](fs: Seq[Future[A]])(implicit ec: ExecutionContext): Future[(Try[A], Seq[Future[A]])] = {
    @tailrec
    def stripe(p: Promise[(Try[A], Seq[Future[A]])],
               heads: Seq[Future[A]],
               elem: Future[A],
               tail: Seq[Future[A]]): Future[(Try[A], Seq[Future[A]])] = {
      elem.onComplete({ res => if (!p.isCompleted) p.trySuccess((res, heads ++ tail)) })
      if (tail.isEmpty) p.future
      else stripe(p, heads :+ elem, tail.head, tail.tail)
    }

    if (fs.isEmpty) Future.failed(new IllegalArgumentException("empty future list!"))
    else stripe(Promise(), fs.genericBuilder[Future[A]].result(), fs.head, fs.tail)
  }

  def run(current: Seq[FutureResultRest[R]], subscriber: Subscriber[TreeEvent[R]]): Unit = {
    select(current) onSuccess {
      case (done: Try[ResultRest[R]], rest: Seq[FutureResultRest[R]]) =>
        val result = done.get
        val res: TreeResult[R] = result._1
        val remaining = rest ++ result._2.map({ action =>
          subscriber.onNext(TreeActionQueue(action))
          action
        }).map(buildFuture(_, subscriber))
        if (remaining.isEmpty)
          executionContext.shutdown()
        else run(remaining, subscriber)
    }

  }

  def traverse(forest: Seq[TreeAction[R]]): Observable[TreeEvent[R]] =
    Observable[TreeEvent[R]](
      subscriber => {

        val batchSize= 16
        def runBatch(batch: Seq[TreeAction[R]]): Unit = {
          val (part, rest) = batch.splitAt(batchSize)
          run(part.map({ action =>
            subscriber.onNext(TreeActionQueue(action))
            action
          }).map(buildFuture(_, subscriber)), subscriber)
          if (!rest.isEmpty)
            runBatch(rest)
        }

        //        run(forest.map({ action =>
        //          subscriber.onNext(TreeActionQueue(action))
        //          action
        //        }).map(buildFuture(_, subscriber)), subscriber)
        runBatch(forest)
        while (!executionContext.awaitTermination(60, TimeUnit.SECONDS)) {

        }
        subscriber.onCompleted()
      })

  def results(forest: List[TreeAction[R]]): Observable[R] = traverse(forest)
    .filter(te => te.isInstanceOf[TreeActionEnd[R]])
    .map(x => x.asInstanceOf[TreeActionEnd[R]].action.result)

}

object TreeWalker {

  def apply[R](): TreeWalker[R] = new TreeWalker[R](Executors
    .newSingleThreadExecutor())

  def apply[R](threads: Int): TreeWalker[R] = new TreeWalker[R](Executors
    .newFixedThreadPool(threads))

}